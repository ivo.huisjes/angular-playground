import { AbstractControl, ValidationErrors, ValidatorFn } from '@angular/forms';

export class CustomValidators {

  static atLeastOne(control: AbstractControl): ValidatorFn {
    return atLeastOneValidator(control);
  }
}

export function atLeastOneValidator(control: AbstractControl): ValidatorFn {
  return (): ValidationErrors | null => control.value.length > 0 ? null : {requireOne: 'There must be at least one. '};
}
