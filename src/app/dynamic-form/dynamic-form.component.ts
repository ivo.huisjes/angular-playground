import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CustomValidators } from './custom-validators';

@Component({
  selector: 'app-dynamic-form',
  templateUrl: './dynamic-form.component.html',
  styleUrls: ['./dynamic-form.component.css']
})
export class DynamicFormComponent implements OnInit {

  mainForm: FormGroup;
  emailForm: FormGroup;
  notification: string;

  constructor(private formBuilder: FormBuilder) { }

  get emails(): FormArray {
    return this.mainForm.get('emails') as FormArray;
  }

  ngOnInit(): void {
    const emailFormArray = this.formBuilder.array([]);
    this.mainForm = this.formBuilder.group({
      name: [''],
      emails: emailFormArray
    }, {
      validators: CustomValidators.atLeastOne(emailFormArray)
    });

    this.emailForm = this.formBuilder.group({
      email: ['', Validators.email]
    });
  }

  onSubmit(): void {
    this.notification = 'submit success!';
  }

  onSubmitEmail(): void {
    this.emails.push(this.formBuilder.control(this.emailForm.get('email').value, Validators.email));
  }

  onDelete(i: number): void {
    this.emails.removeAt(i);
  }
}
